arpentage
=========

A command line tool to divide a book in multiple parts of equal size.

What is this for?
-----------------

This simple tool is meant to **help organize a specific kind of workshop
called “*Arpentage*”** (\\aʁ.pɑ̃.taʒ\\). The idea is to divide a book in
equal sizes that will be shared among the participants. Each participant
can then read their part before getting together to tell each others
what they’d grasp and how it echoes their
knowledge and practices.

It works even better if the split happens in the middle of chapters.
As, obviously, there is no way you will understand everything
as you're jumping right in the middle of the book, it forces you to stop
trying to understand everything to approach the content from your own
perspectives instead of thinking about what the teacher wants to hear.

It’s best to actually get together in the same place and tear the book
apart. Such a gesture is quite taboo and seeing that nothing is sacred
helps the participants feel on a par with the book.

But this tool is there to help with organizing these kind of workshops
online or with text that were released only in digital formats. Insead
of counting pages, it counts words and does its best to have the same
amount in each part.

To learn more about these kind of workshops, see the [article
written by the collective La
Trouvaille](https://la-trouvaille.org/arpentage/) (in French).

*If you are looking for facilitators, please feel free to
contact [La Dérivation](https://dérivation.fr/).*

Dependencies
------------

 * [Pandoc][] >= 2.0
 * `pdfinfo` from [Poppler][]

On a Debian-based system, you can usually install Pandoc by issuing:

    sudo apt install pandoc poppler-utils

[Pandoc]: https://pandoc.org/
[Poppler]: https://poppler.freedesktop.org/

Usage
-----

`arpentage` can read and write a wide variety of formats (as
long as they are supported by [Pandoc][]). It takes a single file as
input and will output a new file for each part.

Again thanks to Pandoc, the input and output format do not need to be
the same. This veniality comes at a price though: the initial layout and
styling will probably not be kept though, even if both input and output
are in the same format.

The output path is used as a template: `output.html` will become
`output-part-1.html`, `output-part-2.html`, …

`arpentage` can also handle PDF documents. In this case it will
divide the original document into multiple PDF with the same
number of pages in every part.

The command line format is the following:

    arpentage -p|--parts <count> [options] <input>

Example:

    arpentage --parts=6 \
        --output=arpentage/King_Kong_Theorie.odt \
        --append-to-title="Arpentage — partie %d/%d" \
        ebooks/King_Kong_Theorie.epub

Options:

    -p, --parts <count>           number of parts to create
    -f, --from <format>           pandoc format for the source material
    -t, --to <format>             pandoc format for the created parts
    -o, --output <path>           template 
    --append-to-title <template>  string to append at the end of the book
                                  title, in “printf format”, with first
                                  argument being the part number and the
                                  second the number of parts. E.g. “%d/%d"
    -q, --quiet                   suppress output
    -h, --help                    show this text and exit

Implementation notes
--------------------

`arpentage` is mostly implemented as a Pandoc filter.

Since version 2.0, Pandoc accepts [filters written in
Lua](https://pandoc.org/lua-filters.html) which makes it quite easy
to do powerful transformations to a document before its output.
But Pandoc is really meant to process a single document at
a time, both on input and output. This issue is quite easy to
overcome at the price of some extra processing though: let’s
run the whole process for every part we need to create.

Communication from the [main script](./arpentage) to the
[filter](./arpentage-filter.lua) is done using environment variables.

The process is as follow:

 1. As an optimization, we first start by running Pandoc with the filter in
    word counting mode, producing no output.
 2. Then, for each part, we will run Pandoc and the filter so it can
    remove all content that should not end up in this particular part.

When working on PDF files, it will use `pdfinfo` to determine the
title and the number of pages. Pandoc will then be used to drive the
`pdfpages` LaTeX package in order to produce the output PDFs.

Acknowledgements
----------------

Thanks to Mélissa and Manu for teaching me how to facilitate
*arpentages*.

Huge thanks to John MacFarlane and everyone who ever worked
on Pandoc. It is a such a powerful, versatile and amazing
tool.

License
-------

Copyright © 2020 Lunar, La Dérivation

This is free software under the “Expat” or “MIT” license. See
[LICENSE](./LICENSE) for the complete text.

Part of `arpentage-filter.lua` is based on the `wordcount.lua` filter by
John MacFarlane.
