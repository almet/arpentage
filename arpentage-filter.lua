--
-- arpentage: divide a book in multiple parts of equal size
--
-- Copyright © 2020 Lunar, John MacFarlane
-- This is free software under the “Expat” or “MIT” license.
--
-- This is a lua filter for Pandoc >= 2.0.
-- See https://pandoc.org/lua-filters.html for more details.
--
-- Based on the wordcount.lua script by John MacFarlane.
--

-- Constants
local WORDS_PER_MINUTE = 180

-- Parameters through environment variables
local parts = tonumber(os.getenv("PARTS")) or nil
local verbose = tonumber(os.getenv("VERBOSE")) or 0
local current_part = tonumber(os.getenv("CURRENT_PART")) or nil
local words_per_part = tonumber(os.getenv("WORDS_PER_PART")) or nil
local append_to_title = os.getenv("APPEND_TO_TITLE")

-- “Globals”
local word_count = nil

-----------------------------------------------------------------------
-- Word counting functions used in all passes
-- 

local function count_str(el)
  -- we don't count a word if it's entirely punctuation: 
  if el.text:match("%P") then 
      word_count = word_count + 1 
  end 
end

local function count_code(el)
  _,n = el.text:gsub("%S+","") 
  word_count = word_count + n 
end

local function count_code_block(el)
  _,n = el.text:gsub("%S+","") 
  word_count = word_count + n 
end

-----------------------------------------------------------------------
-- Pure word counting:
-- used when CURRENT_PART or WORDS_PER_PART have not been specified.
-- 

local wordcount = { 
  Str = count_str,
  Code = count_code,
  CodeBlock = count_code_block
}

local count_words = {
  Pandoc = function(el) 
    -- Reset our word counter
    word_count = 0
    -- skip metadata, just count body: 
    pandoc.walk_block(pandoc.Div(el.blocks), wordcount) 
    words_per_part = word_count / parts
    if verbose > 0 then
      io.stderr:write(string.format("%d words found!\n", word_count))
      io.stderr:write(string.format("So… we want %d words for each of the %d parts.\n", math.floor(words_per_part), parts))
      io.stderr:write(string.format("Using a conservative estimate of %d words per minute, this means %d minutes to read.\n", WORDS_PER_MINUTE, math.ceil(words_per_part / 180)))
    end
    print(words_per_part)
    os.exit(0)
  end
}

-----------------------------------------------------------------------
-- Filter out unwanted content
-- 

local function maybe_include(el)
  part = math.min(parts, math.floor(word_count / words_per_part) + 1)
  if part == current_part then
    return el
  end
  return {}
end

local function clean_empty(el)
  if pandoc.utils.stringify(el):match("^%s+$") then
    return {}
  end
  return el
end

local filter_other_parts = {
  Str = function(el) 
    count_str(el)
    return maybe_include(el)
  end,

  Code = function(el)
    count_code(el)
    return maybe_include(el)
  end,

  CodeBlock = function(el)
    count_code_block(el)
    return maybe_include(el)
  end,

  Space = maybe_include,
}

local filter = {
  Pandoc = function(el) 
    -- Reset our word counter
    word_count = 0
    -- keep metadata as is, just work on body: 
    new_div = pandoc.walk_block(pandoc.Div(el.blocks), filter_other_parts)
    el.blocks = new_div.content
    return el
  end
}

-----------------------------------------------------------------------
-- Small additions to the result
-- 

local function graft_title(m)
  if append_to_title then
    l = pandoc.List(m.title)
    l:insert(pandoc.Str(string.format(" " .. append_to_title, current_part, parts)))
  end
  return m
end

local fixup = {
  Meta = graft_title
}

-----------------------------------------------------------------------
-- Find out what we needs to run depending on environment variables
-- 

if not parts then
  error("PARTS environment variable is missing or wrong.")
end

if not words_per_part or not current_part then
  return {count_words}
else
  if not words_per_part then
    error("WORDS_PER_PART environment variable is missing or wrong.")
  end
  if not current_part then
    error("CURRENT_PART environment variable is missing or wrong.")
  end
  if current_part > parts then
    error("CURRENT_PART is too big in regards to PARTS")
  end 

  return {filter, fixup}
end
